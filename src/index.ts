import * as express from 'express';
import {config} from 'dotenv';
config();
const port = process.env.PORT;
const app = express();
app.use(express.json());

app.get('/', (req, res) => {
   console.log('Get /')
   res.send('Hello World!');
})

app.post('/test', (req, res) => {
   console.log('Post /test')
   res.json(
      {
         success: true,
         result: {
            test1: 'loshara',
            test2: 'ne loshara',
         },
      }); 
})

app.post('/axios-test', (req, res) => {
   const answer = req.body;

});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })